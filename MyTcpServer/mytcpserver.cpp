#include "mytcpserver.h"
#include <QtDebug>
#include <QCoreApplication>
#define DATABASEMANAGMENTSYSTEM_H

#pragma warning(disable:4996)

QString MyTcpServer::parsingMessage(string message,string *pcmd, string *pa, string *pb, string *pc, string *pd)
{

    unsigned long long pos = message.find("&");
    (*pcmd) = message.substr(0,pos);
    message.erase(0,pos+1);
    pos = message.find("&");
    (*pa) = message.substr(0,pos);
    message.erase(0,pos+1);

    pos = message.find("&");
    (*pb) = message.substr(0,pos);
    message.erase(0,pos+1);

    pos = message.find("&");
    (*pc) = message.substr(0,pos);
    message.erase(0,pos+1);

    pos = message.find("&");
    (*pd) = message.substr(0,pos);
    message.erase(0,pos+1);

    return "0";
}



MyTcpServer::~MyTcpServer()
{
    foreach(int id, sclients.keys()){
        QTextStream os(sclients[id]);
        sclients[id]->close();
        sclients.remove(id);
    }
    server_status = 0;
    mTcpServer->close();
}

MyTcpServer::MyTcpServer(QObject *parent) : QObject(parent) {

    mTcpServer = new QTcpServer(this);

    connect(mTcpServer, &QTcpServer::newConnection,this, &MyTcpServer::slotNewConnection);
    if(!mTcpServer->listen(QHostAddress::Any, 33333))

    {
        qDebug() << "server is not started" ;
    }
    else {
        qDebug() << "server Rabotaet" ;
        server_status=1;
    }


}

void MyTcpServer::slotNewConnection()
{


    if(server_status==1){
            QTcpSocket* clientSocket=mTcpServer->nextPendingConnection();
            int id=(int)clientSocket->socketDescriptor();
            sclients[id]=clientSocket;
            connect(sclients[id], &QTcpSocket::readyRead, this, &MyTcpServer::slotServerRead);
            connect(sclients[id], &QTcpSocket::disconnected, this, &MyTcpServer::slotClientDisconnected);
            currentusers+=1;
            qDebug() << "User join" ;
            qDebug() << "Current Users :" << currentusers ;

        }
}

void MyTcpServer::slotServerRead()
{
    QTcpSocket* clientSocket = static_cast<QTcpSocket*>(sender());
    int id = (int)clientSocket->socketDescriptor();
    QTextStream os(clientSocket);



    if(clientSocket->bytesAvailable()>0)
    {

        QByteArray array=clientSocket->readAll();
        string a = "";
        string b = "";
        string c = "";
        string d = "";
        string cmd="";
        string message;
        message = array.toStdString();


        parsingMessage(message, &cmd, &a, &b, &c, &d);


        dataBaseManagmentSystem bdms;
        if (cmd == "autho")
        {
            array.clear();
            array.append(bdms.authorizeCheck(a,b));
            clientSocket->write(array);

        }
        else if (cmd == "show"){
            vector<datat> users;
            string temp;
            array.clear();
            users=bdms.read();
            temp=bdms.parsingVector(users);
            clientSocket->write(QByteArray::fromStdString(temp));
        }
        else if (cmd == "add"){
            vector<datat> users;
            datat user;
            string id = a;
            string fio = b;
            string tel = c;
            string date = d;
            users=bdms.read();
            users = bdms.dataBaseAdd(users, user, id, fio, tel, date);
            bdms.dataBaseWrite(users);

        }
        else if (cmd == "del"){
            vector<datat> users;
            string id = a;

            users=bdms.read();
            bdms.dataBaseDel(users,id);

        }
        else if (cmd == "find"){
            vector<datat> users;
            vector<datat> users2;
            string id = a;
            string temp;


            users=bdms.read();
            users2 = bdms.dataBaseFindKlient(users, users2, bdms.dataBaseFind(users,id));
            temp=bdms.parsingVector(users2);
            qDebug()<<QByteArray::fromStdString(temp);
            clientSocket->write(QByteArray::fromStdString(temp));


        }
        else if (cmd == "change"){

            vector<datat> users;
            datat user;
            string id = a;
            string fio = b;
            string tel = c;
            string date = d;
            users=bdms.read();
            bdms.dataBaseChange(users,a,b,c,d);


        }

    }

}

void MyTcpServer::slotClientDisconnected()
{
    if(server_status==1){
        QTcpSocket* clientSocket = static_cast<QTcpSocket*>(sender());
        int id=(int)clientSocket->socketDescriptor();
        clientSocket->close();
        currentusers-=1;
        qDebug() << "User leave" ;
        qDebug() << "Current users:" << currentusers ;
    }
}
