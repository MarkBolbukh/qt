#ifndef DATABASEMANAGMENTSYSTEM_H
#define DATABASEMANAGMENTSYSTEM_H
#include <string>
#include <QString>
#include <fstream>
#include <vector>
#include <iostream>
#endif // DATABASEMANAGMENTSYSTEM_H

using namespace std;

struct datat
{
    string id;
    string fio;
    string tel;
    string date;
};
struct logpass
{
    string log;
    string pass;
    string who;
};

class dataBaseManagmentSystem
{
private:

    vector <datat> users;
    logpass user;
public:

    dataBaseManagmentSystem();
    ~dataBaseManagmentSystem();


fstream open_file_klient_read();

fstream open_file_klient_write();

fstream open_file_reg_read();

vector<datat> read();

void parsing(char *buff,const char *a,string *pid,string *pfio, string *ptel, string *pdate);

void parsing_reg(char *buff, const char *a, string *plog, string *ppass, string *pwho);


string parsingVector(vector<datat>);

void dataBaseWrite(vector <datat> );

void dataBaseDel(vector <datat> , string);

void dataBaseChange(vector <datat> , string, string, string, string);

vector <datat> dataBaseFindKlient(vector <datat> , vector <datat>, unsigned long long);

string dataBaseDeparsing(string  , string , string , string );

unsigned long long  dataBaseFind(vector <datat> , string);

vector <datat> dataBaseAdd(vector <datat> , datat  , string , string , string, string );

QString authorizeCheck(string, string);

};
