#include "DBMS.h"
#include <iostream>
#pragma warning(disable:4996)


using namespace std;

fstream dataBaseManagmentSystem::open_file_klient_write()
    {
        fstream a;
        a.open("C:\\Users\\user\\Documents\\MyTcpServer\\database.json", ios::binary | ios::out | ios::trunc);

        return a;
    }

fstream dataBaseManagmentSystem::open_file_klient_read()
{
    fstream a;
    a.open("C:\\Users\\user\\Documents\\MyTcpServer\\database.json", ios::binary | ios::in);

    return a;
}

fstream dataBaseManagmentSystem::open_file_reg_read()
{
    fstream a;
    a.open("C:\\Users\\user\\Documents\\MyTcpServer\\logpass.json", ios::binary | ios::in);

    return a;
}

void dataBaseManagmentSystem::parsing(char *buff,const char *a,string *pid,string *pfio, string *ptel, string *pdate)
{
    char * token;
    int i = 0;

    token = strtok(buff, a);

    while (token!= NULL)
    {
        i += 1;
        token = strtok(NULL, "\"");

        if (i == 3)
            (*pid) = token;
        else if (i == 7)
            (*pfio) = token;
        else if (i == 11)
            (*ptel) = token;
        else if (i == 15)
            (*pdate) = token;
    }
}

void dataBaseManagmentSystem::parsing_reg(char *buff, const char *a, string *plog, string *ppass, string *pwho)
{
    char * token;
    int i = 0;
    string log;
    string pass;
    string who;

    token = strtok(buff, a);

    while (token != NULL)
    {
        i += 1;
        token = strtok(NULL, "\"");

        if (i == 3)
            (*plog) = token;
        else if (i == 7)
            (*ppass) = token;
        else if (i == 11)
            (*pwho) = token;
    }
}

void dataBaseManagmentSystem::dataBaseWrite(vector <datat> users)
{
    fstream file2;
    string temp;

    file2 = open_file_klient_write();
    if (!file2.is_open()) {
        //return 0;
    }

    for (vector<datat>::iterator it = users.begin(); it != users.end(); it++)
    {

        temp = dataBaseDeparsing(it->id, it->fio, it->tel, it->date);

        file2.write(temp.c_str(), temp.size());

        cout << it->id << "  " << it->fio << "  " << it->tel << "  " << it->date << endl;
    }
    //return 0;
}

void dataBaseManagmentSystem::dataBaseDel(vector <datat> users, string id)
{
    vector<datat>::iterator it = users.begin();
    int k = dataBaseFind(users, id);
    users.erase(it + k);
    dataBaseWrite(users);

}

void dataBaseManagmentSystem::dataBaseChange(vector <datat> users, string id, string fio, string tel, string date)
{
    datat test;
    unsigned long long k = dataBaseFind(users, id);
    test = users[k];
    test.fio = fio;
    test.tel = tel;
    test.date = date;

    users[k] = test;
    dataBaseWrite(users);
}

vector <datat> dataBaseManagmentSystem::dataBaseFindKlient(vector <datat> users, vector <datat> users2,unsigned long long k)
{

    users.at(k);
    users2.push_back(users.at(k));
    return users2;
}

vector <datat>  dataBaseManagmentSystem::dataBaseAdd(vector <datat> users, datat test , string id, string fio, string tel, string date)
{
    test.id = id;
    test.fio = fio;
    test.tel = tel;
    test.date = date;
    users.push_back(test);


    return users;
}

unsigned long long dataBaseManagmentSystem::dataBaseFind(vector <datat> users, string id)
{
    unsigned long long k = 0;
    for (vector<datat>::iterator it = users.begin(); it != users.end(); it++)
    {

        k++;
        if (it->id == id)
        {
            break;
        }

    }
    return k-1;
}

QString dataBaseManagmentSystem::authorizeCheck(string log, string pass)
{
    fstream file1;
    char buff[256];
    logpass user;
    QString result;

    file1 = open_file_reg_read();
    if (!file1.is_open()) {
        result = "file error";
        return result;
    }

    while (file1.getline(buff, 256))
    {

        if (buff[0] == '{' || buff[0] == '}')
            continue;

        parsing_reg(buff, "\"", &user.log, &user.pass, &user.who);



        if (log == user.log && pass==user.pass)
        {

            result =QString::fromStdString(user.who);
            return result;
        }

    }

    file1.close();

    return result;
}

string dataBaseManagmentSystem::dataBaseDeparsing(string id , string fio, string tel, string date)
{
    string temp;
    temp.clear();

    temp = "{\n \"id\": \"";
    temp += id;
    temp += "\", \"fio\": \"";
    temp += fio;
    temp += "\", \"tel\": \"";
    temp += tel;
    temp += "\", \"date\": \"";
    temp += date;
    temp += "\"\n}\n";


    return temp;

}

dataBaseManagmentSystem::dataBaseManagmentSystem()
{

}
dataBaseManagmentSystem::~dataBaseManagmentSystem()
{

}

vector<datat> dataBaseManagmentSystem::read() //считывает всю БД из файла
{
vector<datat> temp_db;
fstream file = open_file_klient_read();

char buff[256];
datat temp;
while (file.getline(buff, 256))
    {
        cout << buff << endl;
        if (buff[0] == '{' || buff[0] == '}')
            continue;

        parsing(buff, "\"",&temp.id,&temp.fio,&temp.tel,&temp.date);

        temp_db.push_back(temp);
    }

file.close();

return temp_db;
}

string dataBaseManagmentSystem::parsingVector(vector<datat>db)
{
string temp;

for(vector<datat>::iterator it = db.begin();it!=db.end();it++)
{

temp += it->id + "&" + it->fio + "&" + it->tel +"&" + it->date +"&";
}
return temp;
}
