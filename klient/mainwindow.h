#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <QMainWindow>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include <QString>
#include <QMessageBox>
#include <QStandardItem>
#include <QStandardItemModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void on_pushButtonOK_clicked();
    void slot_connected(int);
    void slot_send_to_server(QString);
    void slot_disconnect();
    QByteArray slot_readyRead(QString, QString);
    void on_pushButtonDataBaseViev_clicked();

private slots:
    void on_pushButtonAdd_clicked();


    void on_pushButtonDel_clicked();

    void on_pushButtonFind_clicked();

    void on_pushButtonChange_clicked();

private:
    QString message;
    QByteArray array;
    QByteArray data;
    QTcpSocket *client_socket;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
