#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string>
#include <Windows.h>
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    client_socket = new QTcpSocket(this);
    client_socket->connectToHost("127.0.0.1",33333);
    connect(client_socket,SIGNAL(connected()),SLOT(slot_connected));
    connect(client_socket,SIGNAL(readyRead()),SLOT(slot_readyRead));
    connect(client_socket,SIGNAL(disconnected()),SLOT(slot_disconnect()));
    ui->frame_2->setVisible(false);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::slot_connected(int socketDescriptor)
{
client_socket = new QTcpSocket(this);
client_socket->setSocketDescriptor(socketDescriptor);

connect(client_socket,SIGNAL(readyRead()),SLOT(slot_readyRead));
connect(client_socket,SIGNAL(disconnected()),SLOT(slot_disconnect()));
}

QByteArray MainWindow::slot_readyRead(QString log,QString pass)
{
    if(client_socket->waitForConnected(500))
        {

        // client_socket->waitForReadyRead(500);
        QByteArray array;
        QString temp;
        QString message="autho&" + log + "&" + pass;
        array=QByteArray::fromStdString(message.toStdString());

        client_socket->write(array);
        client_socket->waitForBytesWritten(200);

        client_socket->waitForReadyRead(200);
        while(client_socket->bytesAvailable() > 0)
        {
            data = client_socket->readAll();
        }

//        return data;
        }

    return data;
}


void MainWindow::slot_send_to_server(QString message)//
{
    message=nullptr;
}
void MainWindow::slot_disconnect()
{
    client_socket->deleteLater();
}

void MainWindow::on_pushButtonOK_clicked()
{
    QMessageBox msgBox;
    QString login = ui->lineEditLogin->text();
    QString password = ui->lineEditPass->text();

    client_socket->connectToHost("127.0.0.1",33333);

    data = slot_readyRead(login,password);

if(data == "admin")
{
    msgBox.setText(data);
    msgBox.exec();
    ui->frame->hide();
    ui->frame_2->setVisible(true);
}
else if(data == "manager")
{
    msgBox.setText(data);
    msgBox.exec();
    ui->frame->hide();
    ui->frame_2->setVisible(true);
}
else if(login =="" || password =="")
{
    msgBox.setText(data);
    msgBox.exec();
    ui->frame->hide();
    ui->frame_2->setVisible(true);
}
else if(data == "login or password is incorrect")
{
    msgBox.setText(data);
    msgBox.exec();
}
else if(data == "")
{

}
else{
    msgBox.setText(data);
    msgBox.exec();
    hide();
}
}

void MainWindow::on_pushButtonDataBaseViev_clicked() // отобразить базу данных
{
    string message = "show&";
    QByteArray temp_array;

    temp_array = QByteArray::fromStdString(message);

    client_socket->write(temp_array);
    client_socket->waitForBytesWritten(200);


    client_socket->waitForReadyRead(200);
    QByteArray array = client_socket->readAll();

    string str = array.toStdString();

    int m = 0;
    int n = 0;
    unsigned long long k = 0;
    QStringList verticalHeader;
    string temp;

    QStandardItemModel *model = new QStandardItemModel;
    QStandardItem *item;

    //Заголовки столбцов
    QStringList horizontalHeader;
    horizontalHeader.append("Id");
    horizontalHeader.append("Name");
    horizontalHeader.append("Tel");
    horizontalHeader.append("Date");

    model->setHorizontalHeaderLabels(horizontalHeader);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->setVisible(false);
    model->setVerticalHeaderLabels(verticalHeader);
    for(int i = 0;str.size();i++)
    {

    k = str.find("&");
    temp = str.substr(0,k);
    str.erase(0,k+1);
    int number = atoi(temp.c_str());
    //Первый ряд
    char *temp_id = new char[256];
    _itoa_s(number,temp_id,256,10);
    item = new QStandardItem(QString(QString::fromUtf8(temp_id)));
    item = new QStandardItem(QString(QString::fromStdString(temp)));
    model->setItem(n, m, item);

    k = str.find("&");
    temp = str.substr(0,k);
    str.erase(0,k+1);

    item = new QStandardItem(QString(QString::fromStdString(temp)));
    model->setItem(n, m+1, item);

    k = str.find("&");
    temp = str.substr(0,k);
    str.erase(0,k+1);

    item = new QStandardItem(QString(QString::fromStdString(temp)));
    model->setItem(n, m+2, item);

    k = str.find("&");
    temp = str.substr(0,k);
    str.erase(0,k+1);

    item = new QStandardItem(QString(QString::fromStdString(temp)));
    model->setItem(n, m+3, item);
    n++;
    }

    ui->tableView->setModel(model);

}

void MainWindow::on_pushButtonAdd_clicked()
{

    QString ID = ui->lineEditId->text();
    string Id = ID.toStdString();


    QString FIO = ui->lineEditName->text();
    string Fio = FIO.toStdString();

    QString TEL = ui->lineEditTel->text();
    string Tel = TEL.toStdString();

    QString DATE = ui->lineEditDate->text();
    string Date = DATE.toStdString();

    string message="add&" + Id + "&" + Fio + "&" + Tel + "&" + Date + "&";
    QByteArray array=QByteArray::fromStdString(message);

    client_socket->write(array);
    client_socket->waitForBytesWritten(200);

    client_socket->waitForReadyRead(1000);
    data = "";
    data = client_socket->readAll();

    ui->lineEditId->clear();
    ui->lineEditName->clear();
    ui->lineEditTel->clear();
    ui->lineEditDate->clear();

    Sleep(2000);
    ui->pushButtonDataBaseViev->click();


    QMessageBox msgBox;
    msgBox.setText(data);
    msgBox.exec();
}

void MainWindow::on_pushButtonDel_clicked()
{
    QString ID = ui->lineEditId->text();
    string Id = ID.toStdString();
    string message="del&" + Id + "&";
    QByteArray array=QByteArray::fromStdString(message);
    client_socket->write(array);
    client_socket->waitForBytesWritten(200);
    client_socket->waitForReadyRead(1000);
    data = "";
    data = client_socket->readAll();
    ui->lineEditId->clear();

    Sleep(2000);
    ui->pushButtonDataBaseViev->click();

    QMessageBox msgBox;
    msgBox.setText(data);
    msgBox.exec();

}

void MainWindow::on_pushButtonFind_clicked()
{
    QString ID = ui->lineEditId->text();
    string Id = ID.toStdString();
    string message="find&" + Id + "&";
    QByteArray array=QByteArray::fromStdString(message);
    client_socket->write(array);
    client_socket->waitForBytesWritten(200);

    Sleep(2000);
    client_socket->waitForReadyRead(200);
    array.clear();
    array = client_socket->readAll();


    string str = array.toStdString();

    int m = 0;
    int n = 0;
    size_t k = 0;
    QStringList verticalHeader;
    string temp;

    QStandardItemModel *model = new QStandardItemModel;
    QStandardItem *item;

    //Заголовки столбцов
    QStringList horizontalHeader;
    horizontalHeader.append("Id");
    horizontalHeader.append("Name");
    horizontalHeader.append("Tel");
    horizontalHeader.append("Date");

    model->setHorizontalHeaderLabels(horizontalHeader);
    model->setVerticalHeaderLabels(verticalHeader);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->setVisible(false);
    for(int i = 0;str.size();i++)
    {

    k = str.find("&");
    temp = str.substr(0,k);
    str.erase(0,k+1);
    int number = atoi(temp.c_str());
    //Первый ряд
    char *temp_id = new char[256];
    _itoa_s(number,temp_id,256,10);
    item = new QStandardItem(QString(QString::fromUtf8(temp_id)));
    item = new QStandardItem(QString(QString::fromStdString(temp)));
    model->setItem(n, m, item);

    k = str.find("&");
    temp = str.substr(0,k);
    str.erase(0,k+1);

    item = new QStandardItem(QString(QString::fromStdString(temp)));
    model->setItem(n, m+1, item);

    k = str.find("&");
    temp = str.substr(0,k);
    str.erase(0,k+1);

    item = new QStandardItem(QString(QString::fromStdString(temp)));
    model->setItem(n, m+2, item);

    k = str.find("&");
    temp = str.substr(0,k);
    str.erase(0,k+1);

    item = new QStandardItem(QString(QString::fromStdString(temp)));
    model->setItem(n, m+3, item);
    n++;
    }

    ui->tableView->setModel(model);

    ui->lineEditId->clear();

}

void MainWindow::on_pushButtonChange_clicked()
{
    QString ID = ui->lineEditId->text();
    string Id = ID.toStdString();


    QString FIO = ui->lineEditName->text();
    string Fio = FIO.toStdString();

    QString TEL = ui->lineEditTel->text();
    string Tel = TEL.toStdString();

    QString DATE = ui->lineEditDate->text();
    string Date = DATE.toStdString();

    string message="change&" + Id + "&" + Fio + "&" + Tel + "&" + Date + "&";
    QByteArray array=QByteArray::fromStdString(message);

    client_socket->write(array);
    client_socket->waitForBytesWritten(200);
    client_socket->waitForReadyRead(1000);
    data = "";
    data = client_socket->readAll();
    ui->lineEditId->clear();
    ui->lineEditName->clear();
    ui->lineEditTel->clear();
    ui->lineEditDate->clear();

    Sleep(2000);
    ui->pushButtonDataBaseViev->click();

    QMessageBox msgBox;
    msgBox.setText(data);
    msgBox.exec();

}
