#include "mytcpserver.h"
#include <QtDebug>
#include <QCoreApplication>
#include <sql.h>
#define DATABASEMANAGMENTSYSTEM_H

#pragma warning(disable:4996)


QString MyTcpServer::parsingMessage(string message,string *pcmd, string *pa, string *pb, string *pc, string *pd)
{

    unsigned long long pos = message.find("&");
    (*pcmd) = message.substr(0,pos);
    message.erase(0,pos+1);
    pos = message.find("&");
    (*pa) = message.substr(0,pos);
    message.erase(0,pos+1);

    pos = message.find("&");
    (*pb) = message.substr(0,pos);
    message.erase(0,pos+1);

    pos = message.find("&");
    (*pc) = message.substr(0,pos);
    message.erase(0,pos+1);

    pos = message.find("&");
    (*pd) = message.substr(0,pos);
    message.erase(0,pos+1);

    return "0";
}



MyTcpServer::~MyTcpServer()
{
    foreach(int id, sclients.keys()){
        QTextStream os(sclients[id]);
        sclients[id]->close();
        sclients.remove(id);
    }
    server_status = 0;
    mTcpServer->close();
}

MyTcpServer::MyTcpServer(QObject *parent) : QObject(parent) {

    mTcpServer = new QTcpServer(this);

    connect(mTcpServer, &QTcpServer::newConnection,this, &MyTcpServer::slotNewConnection);
    if(!mTcpServer->listen(QHostAddress::Any, 33333))

    {
        qDebug() << "server is not started" ;
    }
    else {
        qDebug() << "server Rabotaet" ;
        server_status=1;
    }


}

void MyTcpServer::slotNewConnection()
{


    if(server_status==1){
            QTcpSocket* clientSocket=mTcpServer->nextPendingConnection();
            int id=(int)clientSocket->socketDescriptor();
            sclients[id]=clientSocket;
            connect(sclients[id], &QTcpSocket::readyRead, this, &MyTcpServer::slotServerRead);
            connect(sclients[id], &QTcpSocket::disconnected, this, &MyTcpServer::slotClientDisconnected);
            currentusers+=1;
            qDebug() << "User join" ;
            qDebug() << "Current Users :" << currentusers ;

        }
}

void MyTcpServer::slotServerRead()
{
    QTcpSocket* clientSocket = static_cast<QTcpSocket*>(sender());
    int id = (int)clientSocket->socketDescriptor();
    QTextStream os(clientSocket);



    if(clientSocket->bytesAvailable()>0)
    {

        QByteArray array=clientSocket->readAll();
        string a = "";
        string b = "";
        string c = "";
        string d = "";
        string cmd="";
        string message;
        message = array.toStdString();


        parsingMessage(message, &cmd, &a, &b, &c, &d);

        if (cmd == "autho")
        {
            SqlBase obj;
            string temp;
            temp = obj.autho(a,b);
            clientSocket->write(QByteArray::fromStdString(temp));
            obj.baseClose();

        }

        else if (cmd == "show"){
            QString temp;
            SqlBase obj;
            array.clear();
            temp = obj.showAll();
            clientSocket->write(QByteArray::fromStdString(temp.toStdString()));
            obj.baseClose();

        }

        else if (cmd == "add"){

            QString temp;
            SqlBase obj;
            temp = obj.add(a, b, c, d);
            clientSocket->write(QByteArray::fromStdString(temp.toStdString()));
            obj.baseClose();
        }

        else if (cmd == "del"){

            QString temp;
            SqlBase obj;
            temp = obj.del(a);
            clientSocket->write(QByteArray::fromStdString(temp.toStdString()));
            obj.baseClose();
        }

        else if (cmd == "find"){

            QString temp;
            SqlBase obj;
            temp = obj.find(a);
            clientSocket->write(QByteArray::fromStdString(temp.toStdString()));
            obj.baseClose();

        }

        else if (cmd == "change"){

            QString temp;
            SqlBase obj;
            temp = obj.change(a, b, c, d);
            clientSocket->write(QByteArray::fromStdString(temp.toStdString()));
            obj.baseClose();

        }






    }
}

void MyTcpServer::slotClientDisconnected()
{
    if(server_status==1){
        QTcpSocket* clientSocket = static_cast<QTcpSocket*>(sender());
        int id=(int)clientSocket->socketDescriptor();
        clientSocket->close();
        currentusers-=1;
        qDebug() << "User leave" ;
        qDebug() << "Current users:" << currentusers ;
    }
}
