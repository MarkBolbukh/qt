#include <sql.h>


QSqlDatabase SqlBase::baseOpen(QSqlDatabase db)
{

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("C:\\Users\\user\\Documents\\MyTcpServer\\logpass.sqlite");

    if(!db.open())
    {
        qDebug() << "Error";
    }
    else {
        qDebug() << "OK";
    }
   return db;
}

void SqlBase::baseClose()
{
    QSqlDatabase db;
    db.close();
    db.removeDatabase(QSqlDatabase::defaultConnection);
}

string SqlBase::autho(string log, string pass)
{

    QSqlDatabase db;
    db = baseOpen(db);

    QString tlog,tpass;
    tlog = QString::fromStdString(log);
    tpass = QString::fromStdString(pass);
    QSqlQuery query;
    string status;

    if(query.exec("select * from logpass where login='"+tlog +"' and password='"+tpass+"'"))
    {
        int i = 0;
        while(query.next())
        {
            i++;
            QString temp = query.value(2).toString();
            status = temp.toStdString();
        }
        if(i==1)
        {
            baseClose();
            return status;
        }
        if(i>1)
        {
            baseClose();
            return "Есть повторы";
        }
        if(i < 1)
        {
            baseClose();
            return "Неверный логин или пароль";
        }
    }
}

string SqlBase::checkFio(string fio)//Используемые символы - только буквы; Если регистр не тот, меняем на правильный.
{

    for(unsigned long long i = 0; i < fio.size();i++)
    {
        if(fio[i] < 65 || fio[i] > 122 ||  !isalpha(fio[i]) )
        {
            fio.erase(i,1);
            i--;
            continue;
        }
        if(fio[0] > 97 && fio[0] < 122)
        {
            fio[0]-=32;
            continue;
        }
        if(fio[i] >= 65 && fio[i] <= 90)
        {
            fio[i]+=32;
        }
        if(fio[i] >= 97 && fio[i] < 122 )
        {
            continue;
        }


    }
        return fio;
}
string SqlBase::checkTel(string tel)//длина строки 10 символов, все из которых цифры;
{
    int k = 0;
    for(unsigned long long i = 0;i < tel.size();i++)
    {
        if(isdigit(tel[i]))
        {
            k++;
        }
        else {
            return "null";
        }
    }
    if(k == 10)
    {
        return tel;
    }
    else {
        return "null";
    }
}




QString SqlBase::showAll()
{
    QSqlDatabase db;
    db = baseOpen(db);

    QSqlQuery qry("select * from user order by id");
    QString result;
    if(qry.exec())
    {
        while(qry.next())
        {
            QString id = qry.value(0).toString();
            QString name = qry.value(1).toString();
            QString tel = qry.value(2).toString();
            QString date = qry.value(3).toString();
            result += id + "&" + name + "&" + tel +"&" + date +"&";
        }
        baseClose();
        return result;
    }
    else
    {
        baseClose();
        return Error(qry).text();
    }
}

QString SqlBase::add(string tempId, string tempName, string tempTel, string tempDate)
{

    QString id = QString::fromStdString(tempId);
    QString name = QString::fromStdString(checkFio(tempName));
    QString tel = QString::fromStdString(checkTel(tempTel));
    QString date = QString::fromStdString(tempDate);

    QSqlDatabase dataBase;
    dataBase = baseOpen(dataBase);

    QSqlQuery qry;
    qry.prepare("insert into user (id,name,tel,date) values ('"+id+"','"+name+"','"+tel+"','"+date+"')");

    if(qry.exec())
    {
        baseClose();
        return "Succesfully";
    }
    else {
        baseClose();
        return Error(qry).text();
    }


}

QString SqlBase::del(string tempId)
{
    QSqlDatabase dataBase;
    dataBase = baseOpen(dataBase);
    QSqlQuery qry;
    QString id = QString::fromStdString(tempId);


    qry.prepare("Delete from user where id = '"+id+"'");


    if(qry.exec())
    {
        baseClose();
        return "Пользователь удален";
    }
    else {
        baseClose();
        return Error(qry).text();
    }


}

QString SqlBase::find(string tempId)
{
    QSqlDatabase dataBase;
    dataBase = baseOpen(dataBase);
    QString id = QString::fromStdString(tempId);
    QSqlQuery qry("Select * from user where id = '"+id+"'");
    if(qry.exec())
    {
        QString result;

        while(qry.next())
        {
            QString id = qry.value(0).toString();
            QString name = qry.value(1).toString();
            QString tel = qry.value(2).toString();
            QString date = qry.value(3).toString();
            result += id + "&" + name + "&" + tel +"&" + date +"&";
        }
        baseClose();
        return result;
    }
    else {
        baseClose();
        return Error(qry).text();
    }

}

QString SqlBase::change(string tempId, string tempName, string tempTel, string tempDate)
{

    QString id = QString::fromStdString(tempId);
    QString name = QString::fromStdString(checkFio(tempName));
    QString tel = QString::fromStdString(checkTel(tempTel));
    QString date = QString::fromStdString(tempDate);

    QSqlDatabase dataBase;
    dataBase = baseOpen(dataBase);
    QSqlQuery qry;

    qry.prepare("update user set id ='"+id+"',name ='"+name+"',tel = '"+tel+"',date = '"+date+"' where id = '"+id+"'");

    if(qry.exec())
    {
        baseClose();
        return "Пользователь изменен";
    }
    else {
        baseClose();
        return Error(qry).text();
    }
}




QSqlError SqlBase::Error(QSqlQuery qry)
{
    return qry.lastError();
}
