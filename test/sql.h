#ifndef SQL_H
#define SQL_H
#include <QtSql>
#include "QSqlQuery"
#include <string>
using namespace std;


struct users
{
    int id;
    string fio;
    string tel;
    string date;
};
struct logpass
{
    string log;
    string pass;
    string who;
};


class SqlBase {

public:
     vector <users> dataBase;
     logpass user;

     string checkFio(string);
     string checkTel(string);

     string autho(string, string);
     QSqlDatabase baseOpen(QSqlDatabase);
     void baseClose();
     QString showAll();
     QSqlError Error(QSqlQuery);
     QString add(string, string, string, string);
     QString del(string);
     QString find(string);
     QString change(string, string, string, string);



 };




#endif // SQL_H
